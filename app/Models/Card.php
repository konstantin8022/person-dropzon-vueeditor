<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;

    
     protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable =['name','list_id', 'status',  'price', 'created_at',
        'updated_at',
        'deleted_at', ];

     public function tasks(){
    	return $this->hasMany(Task::class);
    }


}
