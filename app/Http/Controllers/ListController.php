<?php

namespace App\Http\Controllers;

use App\Models\DeskList;
use Illuminate\Http\Request;
use App\Http\Resources\DeskListResource;
use Illuminate\Http\Response;
use App\Http\Requests\DeskListStoreRequest;
use App\Http\Requests\DeskListUpdateRequest;
use Illuminate\Support\Facades\DB;
class ListController extends Controller
{
    
    public function index(Request $request)
    {
       /* $items = DB::table("desk_lists")
                ->select("desk_lists.id","desk_lists.name"
                    
                    ,DB::raw('SUM(cards.price) as total_price'))
                ->join("cards","cards.list_id","=","desk_lists.id")
                ->groupBy("desk_lists.id")
              // ->having("total_quantity","<",DB::raw("items.min_quantity"))
                ->get();
        */        
        

        $desklist = DeskList::withSum('cards','price')->get();
        foreach ($desklist as $list) {
            $list->current_sum =$list->cards_sum_price;
            $list->save();
        }       

      /*   dd($items);
       $desklists=DeskList::has('cards')->get();
            foreach($desklists as $list){
                
                foreach ($items as $item) {
                    if($list->id == $item->id){
                        $list->current_sum=$item->total_price;
                        $list->save();
                    }      
                }
                
            }


            $desklists1=DeskList::doesntHave('cards')->get();
               foreach ($desklists1 as $lst) {
                   
                    $lst->current_sum =0 ;
                    $lst->save();
                }
        */    
            //$request->validate(['desk_id' => 'required|integer|exists:desks,id ']); // not work
        // $request->validate(['desk_id' => 'required|integer']);
            //return DeskList::all();
           // return DeskListResource::collection(DeskList::with('cards')->get());
          //  return DeskListResource::collection(DeskList::orderBy('created_at','desc')->
            //       where('desk_id', $request->desk_id)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeskListStoreRequest $request)
    {
    

        $rez=DeskList::create($request->validated());
      //$rez=DeskList::create($request->all());
        return new  DeskListResource($rez);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DeskList  $deskList
     * @return \Illuminate\Http\Response
     */
   // public function show($id)
   // public function show(DeskList $id)
   // {
      //  return DeskList::findOrFail($id);

   // return new DeskListResource( DeskList::findOrFail($id));
       // return new DeskListResource($deskList);
   // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DeskList  $deskList
     * @return \Illuminate\Http\Response
     */
    public function update(DeskListUpdateRequest $request, $id)
    {
       //dd($request->all());
     $updaterez=DeskList::find($id);
      $updaterez->update($request->validated());

        return  new DeskListResource($updaterez ); //$desk

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DeskList  $deskList
     * @return \Illuminate\Http\Response
     */
   public function destroy($id)
     //public function destroy(DeskList $deskList)
    {
        $delrez=DeskList::find($id);

       $delrez->delete();
       // $deskList -> delete();
        return response(null, Response::HTTP_NO_CONTENT );
    }
}
