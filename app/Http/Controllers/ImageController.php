<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\UpdateImageRequest;
use App\Http\Requests\ImageUploadRequest;
use App\Models\Post;
use App\Models\Image;
use Carbon\Carbon;
use App\Http\Resources\PostResource;
use Illuminate\Support\Facades\Storage;
class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::latest()->first();
         return new PostResource($post);
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //EditorImageRequest
     public function storeditor(ImageUploadRequest $request)
    {
            $data=$request->validated();
            //dd($data);
            $image=$data['image'];
            $name=md5( Carbon::now().'_'.$image->getClientOriginalName()).'.'.$image->getClientOriginalExtension(); 
            $filePath=Storage::disk('public')->putFileAs('/images/content',$image ,$name);
            

         return response()->json([ 'url' => url( '/storage/' .$filePath), ]);    
    }


    

    public function store(ImageRequest $request)
    {

        $data = $request->validated();
        $images = $data['images'];
        unset($data['images']);
        $post=Post::firstOrCreate($data);
        
        foreach($images as $image){
            $name=md5( Carbon::now().'_'.$image->getClientOriginalName()).'.'.$image->getClientOriginalExtension(); 
            $filePath=Storage::disk('public')->putFileAs('/images',$image ,$name);
            //dd($filePath);
            $preview_file='previw_'.$name;
            Image::create([
                'path' => $filePath,
                'url' => url( '/storage/' .$filePath),
                'preview_img' => url( '/storage/images/' .$preview_file),
                'post_id' => $post->id
            ]);
            \Intervention\Image\Facades\Image::make($image)->fit(100,100)
            ->save(storage_path('app/public/images/'.$preview_file));

        }
        return response()->json(['message' => 'success']);
    }




   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateImageRequest $request, Post $post)
    {
        $data = $request->validated();
        
        $images = isset( $data['images']) ? $data['images'] : null ;
        $imagesIdsForDel = isset( $data['image_ids_for_delete']) ? $data['image_ids_for_delete'] : null ;  
        //video 10 
         $imagesUrlsForDel = isset( $data['image_urls_for_delete']) ? $data['image_urls_for_delete'] : null ;  
       // dd($imagesUrlsForDel);
      
       
        unset($data['images'],$data['image_ids_for_delete'], $data['image_urls_for_delete'] );
        $post->update($data);
        

        $currentImages = $post->images;
        if( $imagesIdsForDel){
              foreach ($currentImages as $currentImage) {
                if(in_array($currentImage->id,$imagesIdsForDel )){
                       Storage::disk('public')->delete($currentImage->path); 
                       Storage::disk('public')->delete( str_replace('images/','images/previw_',  $currentImage->path )); 
                       $currentImage->delete();
                }
            }
        }
        if($imagesUrlsForDel){
            foreach ($imagesUrlsForDel as $imageUrlDel) {
                $removeStr = $request->root().'/storage/';
                $path=str_replace($removeStr,'',$imageUrlDel );
                //dd($path);
                Storage::disk('public')->delete($path); 
               
            }

        }
       

        if( $images){
            foreach($images as $image){
                $name=md5( Carbon::now().'_'.$image->getClientOriginalName()).'.'.$image->getClientOriginalExtension(); 
                $filePath=Storage::disk('public')->putFileAs('/images',$image ,$name);
                //dd($filePath);
                $preview_file='previw_'.$name;
                Image::create([
                    'path' => $filePath,
                    'url' => url( '/storage/' .$filePath),
                    'preview_img' => url( '/storage/images/' .$preview_file),
                    'post_id' => $post->id
                ]);
                \Intervention\Image\Facades\Image::make($image)->fit(100,100)
                ->save(storage_path('app/public/images/'.$preview_file));

            }
        }

        return response()->json(['message' => 'success']);

    }

   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
