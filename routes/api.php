<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DeskController;

use App\Http\Controllers\ListController;
use App\Http\Controllers\CardController;

use App\Http\Controllers\TaskController;

use App\Http\Controllers\ImageController;

use App\Http\Controllers\PersonController;


/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/
Route::get('status/{id}', [CardController::class, 'status']);
Route::post('storeditor', [ImageController::class, 'storeditor']);

Route::patch('images/{post}', [ImageController::class, 'update']);

Route::apiResources([

    'desks' => DeskController::class,
    'lists' => ListController::class,
    'cards' => CardController::class,
    'tasks' => TaskController::class,

    'images' => ImageController::class,

    'persons' => PersonController::class,


]);

