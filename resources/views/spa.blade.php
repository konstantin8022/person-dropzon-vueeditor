<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<link rel="stylesheet"  href="{{ mix('/css/app.css') }} ">
	<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js"></script>
	<link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
</head>
<body>
	<div id="app" class="m-auto">
		<app></app>
	</div>
	<script src="{{mix('js/app.js')}}"></script>
</body>
</html>